package adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kanika.channelapp.BR;
import com.example.kanika.channelapp.R;

import java.util.List;

import models.ChannelBriefModel;
import viewModels.itemViewModel.ChannelItemViewModel;

/**
 * Created by kanika on 03/12/17.
 *
 * Adapter for displaying the data of channels with minimal info on the first screen - HomeActivity
 */

public class ChannelBriefAdapter extends RecyclerView.Adapter<ChannelBriefAdapter.ChannelBriefViewHolder> {

    private List<ChannelBriefModel> mChannels;
    private ChannelItemViewModel channelItemViewModel;

    @Override
    public ChannelBriefViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.channel_brief_layout,parent,false);
        return new ChannelBriefViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ChannelBriefViewHolder holder, int position) {
        ChannelBriefModel channel = mChannels.get(position);
        channelItemViewModel = new ChannelItemViewModel(channel, holder.itemView.getContext());
        holder.getBinding().setVariable(BR.channel, channelItemViewModel);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mChannels == null ? 0 : mChannels.size();
    }

    public void setChannels(List<ChannelBriefModel> channels) {
        this.mChannels = channels;
        notifyDataSetChanged();
    }

    public class ChannelBriefViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public ViewDataBinding getBinding() {
            return binding;
        }

        public ChannelBriefViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
