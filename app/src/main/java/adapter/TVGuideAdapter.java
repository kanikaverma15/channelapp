package adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kanika.channelapp.R;
import com.example.kanika.channelapp.BR;

import java.util.LinkedHashMap;
import java.util.List;

import models.EventModel;
import viewModels.itemViewModel.EventListItemViewModel;

/**
 * Created by kanika on 04/12/17.
 *
 * Adapter to display the data of TV guide with channel and thw show it is airing currently
 */

public class TVGuideAdapter extends RecyclerView.Adapter<TVGuideAdapter.TVGuideViewHolder> {


    private LinkedHashMap<Long, List<EventModel>> mEvents;
    private EventListItemViewModel eventListItemViewModel;

    @Override
    public TVGuideViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tv_guide_item_layout,parent,false);
        return new TVGuideViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TVGuideViewHolder holder, int position) {
        List<EventModel> events = mEvents.get(mEvents.keySet().toArray()[position]);
        eventListItemViewModel = new EventListItemViewModel(events, holder.itemView.getContext());
        holder.getBinding().setVariable(BR.event, eventListItemViewModel);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mEvents == null ? 0 : mEvents.size();
    }

    public void addEvents(LinkedHashMap<Long, List<EventModel>> events) {
        if(mEvents == null) {
            mEvents = new LinkedHashMap<>();
        }
        int initialSize = mEvents.size();
        mEvents.putAll(events);
        notifyItemRangeInserted(initialSize, mEvents.size());
    }

    public void resetData() {
        if(mEvents != null)
            mEvents.clear();
        notifyDataSetChanged();
    }

    public class TVGuideViewHolder extends RecyclerView.ViewHolder {
        private ViewDataBinding binding;

        public ViewDataBinding getBinding() {
            return binding;
        }

        public TVGuideViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
