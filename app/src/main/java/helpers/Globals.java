package helpers;

import models.ChannelListModel;

/**
 * Created by kanika on 05/12/17.
 */

public class Globals {

    public static final int MAX_NO_SHOWS = 2;
    public static final int IDS_COUNT_FOR_QUERY = 10;

    public static SORT_TYPE currentSort = SORT_TYPE.BY_NAME;  // true for sort By number, false by sort by name

    private static ChannelListModel sortedByNumber;
    private static ChannelListModel sortedByName;

    public static ChannelListModel getSortedByName() {
        return sortedByName;
    }

    public static void setSortedByName(ChannelListModel sortedByName) {
        Globals.sortedByName = sortedByName;
    }

    public static ChannelListModel getSortedByNumber() {
        return sortedByNumber;
    }

    public static void setSortedByNumber(ChannelListModel sortedByNumber) {
        Globals.sortedByNumber = sortedByNumber;
    }

    public static enum SORT_TYPE {
        BY_NAME,
        BY_NUMBER
    }
}
