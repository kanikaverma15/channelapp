package helpers;

import android.content.Context;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import events.NavigationEvent;

/**
 * Created by kanika on 03/12/17.
 *
 * Navigation helper class
 */

public class NavigationManager {
    private Context context;

    public NavigationManager(Context context) {
        this.context = context;
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void onNavigateEvent(NavigationEvent event) {
        context.startActivity(event.getIntent());
    }
}
