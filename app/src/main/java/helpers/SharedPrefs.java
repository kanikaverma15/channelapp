package helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by kanika on 03/12/17.
 */

public class SharedPrefs {

    private static SharedPreferences preferences;
    private static String KEY_FAVOURITES = "favourites";
    public static String LAST_TIME_ZONE = "last_time_zone";

    public static SharedPreferences getPreferences() {
        return preferences;
    }

    public static void init(Context context) {
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void put(String key, Boolean value) {
        if(value == null)
            getPreferences().edit().remove(key).apply();
        else
            getPreferences().edit().putBoolean(key, value).apply();
    }

    public static void put(String key, Float value) {
        if(value == null)
            getPreferences().edit().remove(key).apply();
        else
            getPreferences().edit().putFloat(key, value).apply();
    }

    public static void put(String key, Long value) {
        if(value == null)
            getPreferences().edit().remove(key).apply();
        else
            getPreferences().edit().putLong(key, value).apply();
    }

    public static void put(String key, Integer value) {
        if(value == null)
            getPreferences().edit().remove(key).apply();
        else
            getPreferences().edit().putInt(key, value).apply();
    }

    public static void put(String key, String value) {
        if(TextUtils.isEmpty(value))
            getPreferences().edit().remove(key).apply();
        else
            getPreferences().edit().putString(key, value).apply();
    }

    public static void clear() {
        getPreferences().edit().clear().apply();
    }


    public static void updateFavorites(ArrayList<Integer> value) {
        JSONArray jsonArray = new JSONArray(value);
        put(KEY_FAVOURITES, jsonArray.toString());
    }

    public static ArrayList<Integer> getFavourites() {
        ArrayList<Integer> result = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(getPreferences().getString(KEY_FAVOURITES, "[]"));
            for (int i = 0; i < jsonArray.length(); i++) {
                result.add((Integer) jsonArray.get(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean isFavourite(String channelId) {
        boolean isFav = false;
        if(channelId != null) {
            ArrayList<Integer> favs = getFavourites();
            if (favs != null && favs.size() > 0) {
                isFav = favs.contains(Integer.valueOf(channelId));
            }
        }
        return isFav;
    }
}
