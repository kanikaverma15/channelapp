package helpers;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import networkLayer.GsonRequest;

/**
 * Created by kanika on 03/12/17.
 *
 * URL helper class containing various URLs and a few parameter helper methods
 */

public class ServerUrl {
    private static final String BASE_URL = " http://ams-api.astro.com.my/";
    public static final String GET_CHANNELS_BRIEF_LIST = BASE_URL + "ams/v3/getChannelList";
    public static final String GET_TV_GUIDE_DATA = BASE_URL + "ams/v3/getEvents";


    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");

    public static String getPeriodStartDate() {
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getDefault());
        sdf.setTimeZone(TimeZone.getDefault());
        Date date = c.getTime();
        return sdf.format(date);
    }

    public static String getPeriodEndDate() {
        Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getDefault());
        c.add(Calendar.HOUR, 1);
        sdf.setTimeZone(TimeZone.getDefault());
        return sdf.format(c.getTime());
    }
}
