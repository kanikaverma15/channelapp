package helpers;

import android.content.Context;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import events.ToastMessageEvent;

/**
 * Created by kanika on 03/12/17.
 *
 * Toast Manager helper class for initializing ToasrEvent
 */

public class ToastManager {
    private Context mContext;

    public ToastManager(Context context) {
        this.mContext = context;
        EventBus.getDefault().register(this);
    }

    @Subscribe
    public void onToastEvent(ToastMessageEvent messageEvent) {
        Toast.makeText(mContext, messageEvent.getToastMessage(), Toast.LENGTH_SHORT).show();
    }
}
