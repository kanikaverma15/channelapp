package helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.TimeZone;

/**
 * Created by kanika on 05/12/17.
 *
 * BroadcastReceiver to receive TIME_ZONE_CHANGE and TIME_SET
 */

public class TimeZoneChangedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        /* need to bother only when the timezone really changes !*/

        String oldTimezone = SharedPrefs.getPreferences().getString(SharedPrefs.LAST_TIME_ZONE, null);
        String newTimezone = TimeZone.getDefault().getID();

        long now = System.currentTimeMillis();

        if (oldTimezone == null || TimeZone.getTimeZone(oldTimezone).getOffset(now) != TimeZone.getTimeZone(newTimezone).getOffset(now)) {
            SharedPrefs.getPreferences().edit().putString(SharedPrefs.LAST_TIME_ZONE, newTimezone).apply();
            Log.i(this.getClass().getSimpleName() ,"TimeZone time change");
        }

    }
}
