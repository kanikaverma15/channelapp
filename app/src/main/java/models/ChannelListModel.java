package models;

import java.util.List;

/**
 * Created by kanika on 03/12/17.
 */

public class ChannelListModel extends BaseModel {
    private List<ChannelBriefModel> channels;

    public List<ChannelBriefModel> getChannels() {
        return channels;
    }

    public void setChannels(List<ChannelBriefModel> channels) {
        this.channels = channels;
    }
}
