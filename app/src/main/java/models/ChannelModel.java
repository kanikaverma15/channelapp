package models;

import java.util.List;

/**
 * Created by kanika on 03/12/17.
 */

public class ChannelModel extends BaseModel {
    private String channelHD;

    private String channelId;

    private String channelStbNumber;

    private String channelStartDate;

    private String channelTitle;

    private String channelColor2;

    private String channelColor3;

    private String channelCategory;

    private String channelColor1;

    private String channelEndDate;

    private List<ChannelExtRef> channelExtRef;

    private String channelDescription;

    private String channelLanguage;

    private String siChannelId;

    private List<LinearOttMapping> linearOttMapping;

    private String hdSimulcastChannel;

    public String getChannelHD() {
        return channelHD;
    }

    public void setChannelHD(String channelHD) {
        this.channelHD = channelHD;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelStbNumber() {
        return channelStbNumber;
    }

    public void setChannelStbNumber(String channelStbNumber) {
        this.channelStbNumber = channelStbNumber;
    }

    public String getChannelStartDate() {
        return channelStartDate;
    }

    public void setChannelStartDate(String channelStartDate) {
        this.channelStartDate = channelStartDate;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }

    public String getChannelColor2() {
        return channelColor2;
    }

    public void setChannelColor2(String channelColor2) {
        this.channelColor2 = channelColor2;
    }

    public String getChannelColor3() {
        return channelColor3;
    }

    public void setChannelColor3(String channelColor3) {
        this.channelColor3 = channelColor3;
    }

    public String getChannelCategory() {
        return channelCategory;
    }

    public void setChannelCategory(String channelCategory) {
        this.channelCategory = channelCategory;
    }

    public String getChannelColor1() {
        return channelColor1;
    }

    public void setChannelColor1(String channelColor1) {
        this.channelColor1 = channelColor1;
    }

    public String getChannelEndDate() {
        return channelEndDate;
    }

    public void setChannelEndDate(String channelEndDate) {
        this.channelEndDate = channelEndDate;
    }

    public List<ChannelExtRef> getChannelExtRef() {
        return channelExtRef;
    }

    public void setChannelExtRef(List<ChannelExtRef> channelExtRef) {
        this.channelExtRef = channelExtRef;
    }

    public String getChannelDescription() {
        return channelDescription;
    }

    public void setChannelDescription(String channelDescription) {
        this.channelDescription = channelDescription;
    }

    public String getChannelLanguage() {
        return channelLanguage;
    }

    public void setChannelLanguage(String channelLanguage) {
        this.channelLanguage = channelLanguage;
    }

    public String getSiChannelId() {
        return siChannelId;
    }

    public void setSiChannelId(String siChannelId) {
        this.siChannelId = siChannelId;
    }

    public List<LinearOttMapping> getLinearOttMapping() {
        return linearOttMapping;
    }

    public void setLinearOttMapping(List<LinearOttMapping> linearOttMapping) {
        this.linearOttMapping = linearOttMapping;
    }

    public String getHdSimulcastChannel() {
        return hdSimulcastChannel;
    }

    public void setHdSimulcastChannel(String hdSimulcastChannel) {
        this.hdSimulcastChannel = hdSimulcastChannel;
    }
}
