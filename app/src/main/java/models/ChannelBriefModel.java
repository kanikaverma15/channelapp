package models;

/**
 * Created by kanika on 03/12/17.
 */

public class ChannelBriefModel {

    private long channelId;
    private String channelTitle;
    private long channelStbNumber;


    public long getChannelStbNumber() {
        return channelStbNumber;
    }

    public void setChannelStbNumber(long channelStbNumber) {
        this.channelStbNumber = channelStbNumber;
    }

    public long getChannelId() {
        return channelId;
    }

    public void setChannelId(long channelId) {
        this.channelId = channelId;
    }

    public String getChannelTitle() {
        return channelTitle;
    }

    public void setChannelTitle(String channelTitle) {
        this.channelTitle = channelTitle;
    }
}
