package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;

import helpers.Globals;

/**
 * Created by kanika on 04/12/17.
 */

public class TVGuideModel extends BaseModel {

    private List<EventModel> getevent;
    LinkedHashMap<Long, List<EventModel>> eventHashMap;

    public LinkedHashMap<Long, List<EventModel>> getEventHashMap() {

        if (getevent == null)
            return null;

        if (Globals.currentSort == Globals.SORT_TYPE.BY_NAME) {
            Collections.sort(getevent, new Comparator<EventModel>() {
                @Override
                public int compare(EventModel o1, EventModel o2) {
                    return o1.getChannelTitle().compareToIgnoreCase(o2.getChannelTitle());
                }
            });
        } else {
            Collections.sort(getevent, new Comparator<EventModel>() {
                @Override
                public int compare(EventModel o1, EventModel o2) {
                    return Long.valueOf(o1.getChannelStbNumber()).compareTo(o2.getChannelStbNumber());
                }
            });
        }

        LinkedHashMap<Long, List<EventModel>> map = new LinkedHashMap<>();
        for (EventModel e : getevent) {
            if (map.containsKey(e.getChannelId()) && map.get(e.getChannelId()).size() < Globals.MAX_NO_SHOWS) {
                map.get(e.getChannelId()).add(e);
            } else {
                ArrayList<EventModel> eventModels = new ArrayList<>();
                eventModels.add(e);
                map.put(e.getChannelId(), eventModels);
            }
        }
        eventHashMap = map;
        return eventHashMap;
    }

    public void setEventHashMap(LinkedHashMap<Long, List<EventModel>> eventHashMap) {
        this.eventHashMap = eventHashMap;
    }

    public List<EventModel> getGetevent() {
        return getevent;
    }

    public void setGetevent(List<EventModel> getevent) {
        this.getevent = getevent;
    }

}
