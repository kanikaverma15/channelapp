package modules;

import javax.inject.Singleton;

import dagger.Component;
import interactor.ChannelsInteractor;

/**
 * Created by kanika on 03/12/17.
 */

@Singleton
@Component(modules = {AppModule.class})
public interface ApplicationComponent {
}
