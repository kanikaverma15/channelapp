package modules;

import android.content.Context;

import com.example.kanika.channelapp.TVGuideActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import helpers.NavigationManager;
import helpers.ToastManager;
import interactor.TVGuideInteractor;
import viewModels.pageViewModel.TVViewModel;

/**
 * Created by kanika on 04/12/17.
 */

@Module
public class TVActivityModule {
    private TVGuideActivity tvGuideActivity;

    public TVActivityModule(TVGuideActivity tvGuideActivity) {
        this.tvGuideActivity = tvGuideActivity;
    }

    @Provides
    Context provideActivityContext() {
        return this.tvGuideActivity;
    }

    @ActivityScope
    @Provides
    ToastManager provideToastManager(Context context) {
        return new ToastManager(context);
    }

    @ActivityScope
    @Provides
    NavigationManager provideNavigateManager(Context context) {
        return new NavigationManager(context);
    }

    @ActivityScope
    @Provides
    TVViewModel provideTVViewModel(Context context, TVGuideInteractor interactor) {
        return new TVViewModel(context, interactor);
    }

    @ActivityScope
    @Provides
    TVGuideInteractor provideChannelsInteractor() {
        return new TVGuideInteractor(tvGuideActivity);
    }
}
