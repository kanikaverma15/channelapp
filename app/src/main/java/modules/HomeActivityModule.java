package modules;

import android.content.Context;

import com.example.kanika.channelapp.HomeActivity;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import helpers.NavigationManager;
import helpers.ToastManager;
import interactor.ChannelsInteractor;
import viewModels.pageViewModel.HomeViewModel;

/**
 * Created by kanika on 03/12/17.
 */

@Module
public class HomeActivityModule {

    private HomeActivity homeActivity;

    public HomeActivityModule(HomeActivity homeActivity) {
        this.homeActivity = homeActivity;
    }

    @Provides
    Context provideActivityContext() {
        return this.homeActivity;
    }

    @ActivityScope
    @Provides
    ToastManager provideToastManager(Context context) {
        return new ToastManager(context);
    }

    @ActivityScope
    @Provides
    NavigationManager provideNavigateManager(Context context) {
        return new NavigationManager(context);
    }

    @ActivityScope
    @Provides
    HomeViewModel provideHomeViewModel(Context context, ChannelsInteractor interactor, NavigationManager navigationManager) {
        return new HomeViewModel(context, interactor, navigationManager);
    }

    @ActivityScope
    @Provides
    ChannelsInteractor provideChannelsInteractor() {
        return new ChannelsInteractor(homeActivity);
    }
}
