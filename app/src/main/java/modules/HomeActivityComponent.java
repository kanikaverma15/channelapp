package modules;

import com.example.kanika.channelapp.HomeActivity;

import dagger.Component;

/**
 * Created by kanika on 03/12/17.
 */

@ActivityScope
@Component(dependencies = {ApplicationComponent.class},modules = {HomeActivityModule.class})
public interface HomeActivityComponent {
    void inject(HomeActivity baseActivity);
}
