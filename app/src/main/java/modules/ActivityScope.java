package modules;

import javax.inject.Scope;

/**
 * Created by kanika on 03/12/17.
 */

@Scope
public @interface ActivityScope {
}
