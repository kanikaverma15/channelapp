package modules;

import com.example.kanika.channelapp.TVGuideActivity;

import dagger.Component;

/**
 * Created by kanika on 04/12/17.
 */

@ActivityScope
@Component(dependencies = {ApplicationComponent.class},modules = {TVActivityModule.class})
public interface TVActivityComponent {
    void inject(TVGuideActivity tvGuideActivity);

}
