package interactor;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import java.util.Map;

import networkLayer.CNetwork;
import networkLayer.GsonRequest;

/**
 * Created by kanika on 03/12/17.
 */

public class BaseNetworkInteractor {

    protected void doRequest(CRequest request, Context context) {
        CNetwork.getInstance(context).getRequestQueue().add(request);
    }

    protected abstract class ModelResponse<T> implements Response.Listener<T>, Response.ErrorListener {
    }

    protected abstract class JsonResponseListener implements Response.Listener<JSONObject>, Response.ErrorListener {
    }


    protected class CRequest<T> extends GsonRequest<T> {
        public CRequest(int method, String url, Map<String, String> jsonObject, ModelResponse responseListner, Class<T> clazz) {
            super(method, url, clazz, jsonObject, responseListner, responseListner);
        }
    }
}
