package interactor;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import helpers.Globals;
import helpers.ServerUrl;
import models.ChannelBriefModel;
import models.ChannelListModel;
import models.ChannelModel;
import rx.subjects.PublishSubject;

/**
 * Created by kanika on 03/12/17.
 */

public class ChannelsInteractor extends BaseNetworkInteractor {

    public PublishSubject<ChannelListModel> mChannelPublishSubject = PublishSubject.create();
    private ChannelListResponseListener mBriefResponseListener;
    public PublishSubject<ChannelModel> mChannelDetailPublishSubject = PublishSubject.create();
    private ChannelDescListResponseListener mFullResponseListener;
    private Context mContext;

    public ChannelsInteractor(Context mContext) {
        this.mContext = mContext;
        this.mBriefResponseListener = new ChannelListResponseListener();
        this.mFullResponseListener = new ChannelDescListResponseListener();
    }

    /* Sort the channels by name*/
    public void sortByName(ChannelListModel channels) {
        if(channels != null) {
            Collections.sort(channels.getChannels(), new Comparator<ChannelBriefModel>() {
                public int compare(ChannelBriefModel obj1, ChannelBriefModel obj2) {
                    // ## Ascending order
                    return obj1.getChannelTitle().compareToIgnoreCase(obj2.getChannelTitle());
                }
            });
        }
        mChannelPublishSubject.onNext(channels);
    }

    /* Sort the channels by channel number*/
    public void sortByNumber(ChannelListModel channels) {
        if(channels != null) {
            Collections.sort(channels.getChannels(), new Comparator<ChannelBriefModel>() {
                public int compare(ChannelBriefModel obj1, ChannelBriefModel obj2) {
                    // ## Ascending order
                    return Long.valueOf(obj1.getChannelStbNumber()).compareTo(obj2.getChannelStbNumber());
                }
            });
        }
        mChannelPublishSubject.onNext(channels);
    }

    public class ChannelListResponseListener extends ModelResponse<ChannelListModel> {

        @Override
        public void onErrorResponse(VolleyError error) {
            mChannelPublishSubject.onError(error);
        }

        @Override
        public void onResponse(ChannelListModel response) {
            setGlobalSortedCollections(response);
            mChannelPublishSubject.onNext(response);
        }
    }

    /* Set the gloabl sorted collections values so that they can be used to fetch the TV Guide result data on 2nd Screen*/
    private void setGlobalSortedCollections(ChannelListModel response) {
        ChannelListModel copy = new ChannelListModel();
        copy.setChannels(new ArrayList<ChannelBriefModel>(response.getChannels()));

        Collections.sort(copy.getChannels(), new Comparator<ChannelBriefModel>() {
            public int compare(ChannelBriefModel obj1, ChannelBriefModel obj2) {
                // ## Ascending order
                return obj1.getChannelTitle().compareToIgnoreCase(obj2.getChannelTitle());
            }
        });
        Globals.setSortedByName(copy);

        ChannelListModel copy2 = new ChannelListModel();
        copy2.setChannels(new ArrayList<ChannelBriefModel>(response.getChannels()));

        Collections.sort(copy2.getChannels(), new Comparator<ChannelBriefModel>() {
            public int compare(ChannelBriefModel obj1, ChannelBriefModel obj2) {
                // ## Ascending order
                return Long.valueOf(obj1.getChannelStbNumber()).compareTo(obj2.getChannelStbNumber());
            }
        });
        Globals.setSortedByNumber(copy2);
    }

    public void fetchChannelBriefList() {
        CRequest request = new CRequest(Request.Method.GET, ServerUrl.GET_CHANNELS_BRIEF_LIST, null, mBriefResponseListener, ChannelListModel.class);
        doRequest(request, mContext);
    }

    public class ChannelDescListResponseListener extends ModelResponse<ChannelModel> {

        @Override
        public void onErrorResponse(VolleyError error) {
            mChannelDetailPublishSubject.onError(error);
        }

        @Override
        public void onResponse(ChannelModel response) {
            mChannelDetailPublishSubject.onNext(response);
        }
    }
    public void fetchChannelFullList() {
        CRequest request = new CRequest(Request.Method.GET, "", null, mFullResponseListener, ChannelModel.class);
        doRequest(request, mContext);
    }

}
