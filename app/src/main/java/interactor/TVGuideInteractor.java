package interactor;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.VolleyError;

import java.util.Map;

import helpers.Globals;
import helpers.ServerUrl;
import models.TVGuideModel;
import networkLayer.UriBuilder;
import rx.subjects.PublishSubject;

/**
 * Created by kanika on 04/12/17.
 */

public class TVGuideInteractor extends BaseNetworkInteractor {

    public PublishSubject<TVGuideModel> tvGuideModelPublishSubject = PublishSubject.create();
    private TVGuideResponseListener tvGuideResponseListener;
    private Context mContext;

    public TVGuideInteractor(Context mContext) {
        this.mContext = mContext;
        this.tvGuideResponseListener = new TVGuideResponseListener();
    }

    public void fetchTVGuideData(Map<String,Object> params) {
        CRequest request = new CRequest(Request.Method.GET, UriBuilder.getQueryURL(ServerUrl.GET_TV_GUIDE_DATA, params), null, tvGuideResponseListener, TVGuideModel.class);
        doRequest(request, mContext);
    }

    /* TV Guide data with sorting done by Channel Name*/
    public void sortByName() {
        Globals.currentSort = Globals.SORT_TYPE.BY_NAME;
        tvGuideModelPublishSubject.onNext(null);
    }

    /* TV Guide data with sorting done by Channel Number*/
    public void sortByNumber() {
        Globals.currentSort = Globals.SORT_TYPE.BY_NUMBER;
        tvGuideModelPublishSubject.onNext(null);
    }

    public class TVGuideResponseListener extends ModelResponse<TVGuideModel> {

        @Override
        public void onErrorResponse(VolleyError error) {
            tvGuideModelPublishSubject.onError(error);
        }

        @Override
        public void onResponse(TVGuideModel response) {
            tvGuideModelPublishSubject.onNext(response);
        }
    }

}
