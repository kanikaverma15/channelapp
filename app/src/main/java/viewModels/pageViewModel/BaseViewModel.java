package viewModels.pageViewModel;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.android.databinding.library.baseAdapters.BR;

/**
 * Created by kanika on 04/12/17.
 */

public class BaseViewModel extends BaseObservable {

    protected boolean isShowLoaderVisible;
    protected boolean isErrorViewVisible;

    @Bindable
    public boolean getIsErrorViewVisible() {
        return isErrorViewVisible;
    }

    @Bindable
    public boolean getIsShowLoaderVisible() {
        return isShowLoaderVisible;
    }

    public void setIsShowLoaderVisible(boolean isVisible){
        isShowLoaderVisible = isVisible;
        notifyPropertyChanged(BR.isShowLoaderVisible);
    }

    public void setErrorViewVisible(boolean errorViewVisible) {
        isErrorViewVisible = errorViewVisible;
        notifyPropertyChanged(BR.isErrorViewVisible);
    }

    public void onTryAgainClick() {
        setErrorViewVisible(false);
    }
}
