package viewModels.pageViewModel;

import android.content.Context;
import android.databinding.BaseObservable;

import com.example.kanika.channelapp.TVGuideActivity;

import interactor.TVGuideInteractor;
import models.TVGuideModel;

/**
 * Created by kanika on 04/12/17.
 */

public class TVViewModel extends BaseViewModel {
    private final Context mContext;
    private final TVGuideInteractor interactor;

    public TVViewModel(Context context, TVGuideInteractor interactor) {
        this.mContext = context;
        this.interactor = interactor;
    }

    public void onSortByNameClick() {
        interactor.sortByName();
    }

    public void onSortByNumberClick() {
        interactor.sortByNumber();
    }

    @Override
    public void onTryAgainClick() {
        super.onTryAgainClick();
        ((TVGuideActivity)mContext).reloadData();
    }
}
