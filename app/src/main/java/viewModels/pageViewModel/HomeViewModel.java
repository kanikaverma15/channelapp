package viewModels.pageViewModel;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.example.kanika.channelapp.HomeActivity;
import com.example.kanika.channelapp.TVGuideActivity;

import org.greenrobot.eventbus.EventBus;

import events.NavigationEvent;
import helpers.Globals;
import helpers.NavigationManager;
import interactor.ChannelsInteractor;
import models.ChannelListModel;

/**
 * Created by kanika on 03/12/17.
 */

public class HomeViewModel extends BaseViewModel {

    private final Context mContext;
    private final ChannelsInteractor interactor;
    private final NavigationManager navigationManager;

    private ChannelListModel channelListModel;

    public HomeViewModel(Context context, ChannelsInteractor interactor, NavigationManager navigationManager) {
        this.mContext = context;
        this.interactor = interactor;
        this.navigationManager = navigationManager;
    }

    public void setChannelListModel(ChannelListModel channelListModel) {
        this.channelListModel = channelListModel;
    }

    public void onSortByNameClick() {
        interactor.sortByName(channelListModel);
    }

    public void onSortByNumberClick() {
        interactor.sortByNumber(channelListModel);
    }

    public void onGoToTvGuideClick() {
        if(Globals.getSortedByName() == null) {
            interactor.fetchChannelBriefList();
        } else {
            EventBus.getDefault().post(new NavigationEvent(new Intent(mContext, TVGuideActivity.class)));
        }
    }

    @Override
    public void onTryAgainClick() {
        super.onTryAgainClick();
        ((HomeActivity)mContext).loadChannels();
    }
}
