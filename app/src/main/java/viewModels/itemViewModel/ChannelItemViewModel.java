package viewModels.itemViewModel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableField;

import com.example.kanika.channelapp.BR;

import java.util.ArrayList;

import helpers.SharedPrefs;
import models.ChannelBriefModel;

/**
 * Created by kanika on 03/12/17.
 *
 * Item level view model for channels list item
 */

public class ChannelItemViewModel extends BaseObservable {

    private Context mContext;
    private long channelId;
    private String channelTitle;
    private String channelNumber;
    private boolean isFavouriteChannel;

    public ChannelItemViewModel(ChannelBriefModel model, Context context) {
        this.channelTitle = model.getChannelTitle();
        this.channelNumber = String.valueOf(model.getChannelStbNumber());
        this.channelId = model.getChannelId();
        this.mContext = context;
        this.isFavouriteChannel = SharedPrefs.isFavourite(String.valueOf(channelId));
    }

    @Bindable
    public String getChannelTitle() {
        return this.channelTitle;
    }

    @Bindable
    public String getChannelNumber() {
        return channelNumber;
    }

    @Bindable
    public ObservableField<Boolean> getIsFavouriteChannel() {
        return new ObservableField<>(isFavouriteChannel);
    }

    public void onFavoriteBtnClick() {
        ArrayList<Integer> favs = SharedPrefs.getFavourites();
        if (isFavouriteChannel) {
            isFavouriteChannel = false;
            favs.remove(Integer.valueOf((int) channelId));
            SharedPrefs.updateFavorites(favs);
        } else {
            isFavouriteChannel = true;
            favs.add((int) channelId);
            SharedPrefs.updateFavorites(favs);
        }
        notifyPropertyChanged(BR.isFavouriteChannel);
    }
}
