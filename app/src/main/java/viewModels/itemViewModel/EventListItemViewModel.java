package viewModels.itemViewModel;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;

import java.util.List;
import com.example.kanika.channelapp.BR;

import models.EventModel;

/**
 * Created by kanika on 04/12/17.
 */

public class EventListItemViewModel extends BaseObservable {

    private Context mContext;
    private long channelId;
    private String channelTitle;
    private String channelNumber;
    private List<EventModel> eventModels;
    private ObservableArrayList<ObservableField<String>> shows;

    public EventListItemViewModel(List<EventModel> eventModels, Context context) {
        this.channelTitle = eventModels.get(0).getChannelTitle();
        this.channelNumber = String.valueOf(eventModels.get(0).getChannelStbNumber());
        this.channelId = eventModels.get(0).getChannelId();
        this.mContext = context;
        this.eventModels = eventModels;
        setShows();
    }

    @Bindable
    public ObservableField<String> getChannelTitle() {
        return new ObservableField<>(channelTitle);
    }

    @Bindable
    public ObservableField<String> getChannelNumber() {
        return new ObservableField<>(channelNumber);
    }

    @Bindable
    public ObservableArrayList<ObservableField<String>> getShows() {
        return shows;
    }

    public void setShows() {
        shows = new ObservableArrayList<>();
        for (EventModel e: eventModels) {
            shows.add(new ObservableField<String>(e.getProgrammeTitle()));
        }
        notifyPropertyChanged(BR.shows);
    }

}
