package com.example.kanika.channelapp;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import adapter.ChannelBriefAdapter;
import application.ChannelApplication;
import interactor.ChannelsInteractor;
import models.ChannelListModel;
import modules.DaggerHomeActivityComponent;
import modules.HomeActivityComponent;
import modules.HomeActivityModule;
import rx.Observable;
import rx.Observer;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import viewModels.pageViewModel.HomeViewModel;

public class HomeActivity extends AppCompatActivity{

    @Inject
    ChannelsInteractor interactor;

    @Inject
    HomeViewModel homeViewModel;

    HomeActivityComponent homeActivityComponent;

    ChannelBriefAdapter adapter;
    RecyclerView channelListRv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        homeActivityComponent= DaggerHomeActivityComponent.builder()
                .applicationComponent(((ChannelApplication)getApplication()).getApplicationComponent())
                .homeActivityModule(new HomeActivityModule(this))
                .build();
        homeActivityComponent.inject(this);

        ViewDataBinding dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        dataBinding.setVariable(BR.homeVM, homeViewModel);
        dataBinding.setVariable(BR.baseVM, homeViewModel);
        dataBinding.executePendingBindings();

        channelListRv = (RecyclerView) findViewById(R.id.channelListRV);
        GridLayoutManager glm = new GridLayoutManager(this, 3, GridLayoutManager.VERTICAL, false);
        channelListRv.setLayoutManager(glm);
        adapter = new ChannelBriefAdapter();
        channelListRv.setAdapter(adapter);
        interactor.mChannelPublishSubject.subscribe(mBriefChannelObserver);
        loadChannels();
    }

    /* fetch the channels with minimal information */
    public void loadChannels() {
        homeViewModel.setIsShowLoaderVisible(true);
        interactor.fetchChannelBriefList();
    }

    /* obsserver for receiving the channels data published by the publisher */
    Observer<ChannelListModel> mBriefChannelObserver = new Observer<ChannelListModel>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            homeViewModel.setIsShowLoaderVisible(false);
            homeViewModel.setErrorViewVisible(true);
        }

        @Override
        public void onNext(ChannelListModel model) {
            homeViewModel.setIsShowLoaderVisible(false);
            homeViewModel.setErrorViewVisible(false);
            homeViewModel.setChannelListModel(model);
            channelListRv.scrollToPosition(0);
            adapter.setChannels(model.getChannels());
        }
    };
}
