package com.example.kanika.channelapp;

import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.inject.Inject;

import adapter.TVGuideAdapter;
import application.ChannelApplication;
import helpers.Globals;
import helpers.RecyclerViewScrollListener;
import helpers.ServerUrl;
import helpers.SharedPrefs;
import interactor.TVGuideInteractor;
import models.TVGuideModel;
import modules.DaggerTVActivityComponent;
import modules.TVActivityComponent;
import modules.TVActivityModule;
import rx.Observer;
import viewModels.pageViewModel.TVViewModel;

/**
 * Created by kanika on 04/12/17.
 */

public class TVGuideActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    @Inject
    TVGuideInteractor interactor;

    @Inject
    TVViewModel tvViewModel;

    TVActivityComponent tvActivityComponent;

    TVGuideAdapter adapter;
    private RecyclerViewScrollListener scrollListener;
    private int pageNumber = 0;
    RecyclerView channelListRv;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        tvActivityComponent = DaggerTVActivityComponent.builder()
                .applicationComponent(((ChannelApplication) getApplication()).getApplicationComponent())
                .tVActivityModule(new TVActivityModule(this))
                .build();
        tvActivityComponent.inject(this);

        ViewDataBinding dataBinding = DataBindingUtil.setContentView(this, R.layout.activity_tv_guide);
        dataBinding.setVariable(com.example.kanika.channelapp.BR.tvVM, tvViewModel);
        dataBinding.setVariable(com.example.kanika.channelapp.BR.baseVM, tvViewModel);
        dataBinding.executePendingBindings();


        channelListRv = (RecyclerView) findViewById(R.id.channelListRV);
        LinearLayoutManager glm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        channelListRv.setLayoutManager(glm);


        scrollListener = new RecyclerViewScrollListener(glm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                // Triggered only when new data needs to be appended to the list
                // Add whatever code is needed to append new items to the bottom of the list
                pageNumber++;
                loadMore();
            }
        };
        // Adds the scroll listener to RecyclerView
        channelListRv.addOnScrollListener(scrollListener);

        adapter = new TVGuideAdapter();
        channelListRv.setAdapter(adapter);
        interactor.tvGuideModelPublishSubject.subscribe(mTVGuideOObserver);
        loadMore();

        SharedPrefs.getPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    /* fetch TV guide data corresponding to channels in chunks of 1-10 */
    private void loadMore() {
        tvViewModel.setIsShowLoaderVisible(true);
        Map<String, Object> params = new HashMap<>();
        ArrayList<Long> channeIds = getChannelIds();
        params.put("channelId", channeIds);
        params.put("periodStart", ServerUrl.getPeriodStartDate());
        params.put("periodEnd", ServerUrl.getPeriodEndDate());

        interactor.fetchTVGuideData(params);
    }

    /* observer to observe the TV guide data published by the publisher */
    Observer<TVGuideModel> mTVGuideOObserver = new Observer<TVGuideModel>() {
        @Override
        public void onCompleted() {

        }

        @Override
        public void onError(Throwable e) {
            tvViewModel.setIsShowLoaderVisible(false);
            tvViewModel.setErrorViewVisible(true);
        }

        @Override
        public void onNext(TVGuideModel model) {
            tvViewModel.setIsShowLoaderVisible(false);
            tvViewModel.setErrorViewVisible(false);
            if (model == null) {
                reloadData();
            } else {
                adapter.addEvents(model.getEventHashMap());
            }
        }
    };

    /* resets the adapter and recyclerview and fetches TV GUide data afresh*/
    public void reloadData() {
        adapter.resetData();
        channelListRv.scrollToPosition(0);
        pageNumber = 0;
        loadMore();
    }

    private ArrayList<Long> getChannelIds() {
        ArrayList<Long> arrayList = new ArrayList<>();
        if (Globals.currentSort == Globals.SORT_TYPE.BY_NAME) {
            for (int i = 1; i <= Globals.IDS_COUNT_FOR_QUERY; i++) {
                arrayList.add(Globals.getSortedByName().getChannels().get(i + (pageNumber * Globals.IDS_COUNT_FOR_QUERY) - 1).getChannelId());
            }
        } else {
            for (int i = 1; i <= Globals.IDS_COUNT_FOR_QUERY; i++) {
                arrayList.add(Globals.getSortedByNumber().getChannels().get(i + (pageNumber * Globals.IDS_COUNT_FOR_QUERY) - 1).getChannelId());
            }
        }
        return arrayList;
    }

    /* to be called when there is a change in the timezone of the device */
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(SharedPrefs.LAST_TIME_ZONE)) {
            reloadData();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPrefs.getPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }
}
