package application;

import android.app.Application;

import helpers.SharedPrefs;
import modules.AppModule;
import modules.ApplicationComponent;
import modules.DaggerApplicationComponent;

/**
 * Created by kanika on 03/12/17.
 *
 * Application class
 */

public class ChannelApplication extends Application {

    private ApplicationComponent applicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationComponent= DaggerApplicationComponent.builder().appModule(new AppModule(this)).build();
        SharedPrefs.init(this);
    }

    public ApplicationComponent getApplicationComponent()
    {
        return applicationComponent;
    }
}
