package networkLayer;

import android.net.Uri;
import android.text.TextUtils;

import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by kanika on 03/12/17.
 */

public class UriBuilder {
    public static String getQueryURL(String url, Map<String, Object> params) {
        Uri.Builder uriBuilder = Uri.parse(url).buildUpon();
        Set<String> keySet = new TreeSet<>(params.keySet());
        for (String key : keySet) {
            String value = params.get(key).toString();
            if (TextUtils.isEmpty(value)) {
                value = "";
            }
            uriBuilder.appendQueryParameter(key, value);
        }
        return uriBuilder.toString();
    }
}
