package customViews;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.ObservableArrayList;
import android.databinding.ObservableField;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

import com.example.kanika.channelapp.R;

/**
 * Created by kanika on 05/12/17.
 *
 * Custom view to display the data of current airing show in the staggered manner
 */

public class ShowDisplayGrid extends TableRow {

    public ShowDisplayGrid(Context context) {
        super(context);
    }

    public ShowDisplayGrid(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @BindingAdapter("app:columns")
    public static void loadColumns(ShowDisplayGrid row, ObservableArrayList<ObservableField<String>> columns) {
        row.removeAllViews();
        int count = 0;
        for (ObservableField<String> c : columns) {
            if (count == 2)
                break;
            TextView columnTV = (TextView) LayoutInflater.from(row.getContext()).inflate(R.layout.grid_cell_layout, null);
            columnTV.setText(c.get());
            columnTV.setGravity(Gravity.CENTER);
            columnTV.setTextAlignment(TEXT_ALIGNMENT_CENTER);
            if (count == 0)
                columnTV.setBackgroundColor(row.getContext().getResources().getColor(R.color.colorCellBg));
            else
                columnTV.setBackgroundColor(row.getContext().getResources().getColor(R.color.colorSilverGrayLight));
            TableRow.LayoutParams tlp = new LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
            tlp.weight = 1;
            row.addView(columnTV, tlp);
            count++;
        }
    }
}
