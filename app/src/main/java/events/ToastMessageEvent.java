package events;

/**
 * Created by kanika on 03/12/17.
 *
 * Event for displaying toast message to be used with EventBus
 */

public class ToastMessageEvent {
    private String toastMessage;

    public ToastMessageEvent(String toastMessage) {
        this.toastMessage = toastMessage;
    }

    public String getToastMessage() {
        return toastMessage;
    }

    public void setToastMessage(String toastMessage) {
        this.toastMessage = toastMessage;
    }
}
