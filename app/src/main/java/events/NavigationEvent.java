package events;

import android.content.Intent;

/**
 * Created by kanika on 03/12/17.
 *
 * Event for navigating to be used with EventBus
 */

public class NavigationEvent {

    private Intent mIntent;
    public NavigationEvent(Intent intent)
    {
        this.mIntent = intent;
    }

    public Intent getIntent() {
        return mIntent;
    }

    public void setIntent(Intent mIntent) {
        this.mIntent = mIntent;
    }
}
