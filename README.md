# README #

### CHANNEL APP ###
An app that will display the Astro channels with an option to sort them by channel number or channel name. Level 2 contains the TV Guide with the channels and the currently show being aired on them. Again here is an option to sort them by channel name or channel number.
This project has been developed using RxJava + MVVM architecture + Dagger2 + eventBus

### Release note 1.0 ###
	What's New
	First Screen - Showing the channels in a grid fashion with an option to mark any channel as favourite. Further the channels can be sorted by name or by number. A link to see the TV Guide which will be displayed on the second screen.
	Second Screen - Shows TV Guide with the channels and the currently show being aired on them. Again here is an option to sort them by channel name or channel number.

### System Requirements ###
	Minimum sdk - 19

### Outstanding issues ###
	Network issue - retry failed request

### What is this repository for? ###
	Has been developed using RxJava + MVVM architecture + Dagger2 + eventBus


